# Travail pratique 3

Dans ce troisième travail pratique, basé sur celui d'Alexandre Blondin Massé, vous devez concevoir un programme qui charge des données géographiques basées sur le projet [countries](https://github.com/mledoze/countries) et qui effectue certains traitements de ces données.

Le travail peut être réalisé **seul** ou en équipes de **2 à 3 personnes**.

**Attention**. Nous nous réservons la possibilité d'attribuer une note complètement différente (pouvant aller jusqu'à zéro) à un membre d'une équipe dont le travail n'est pas jugé suffisant. Pour cela, nous étudierons la division des tâches et les commits (quantité et qualité) qui ont été produits par chacun des membres (voir le phénomène du [passager clandestin](https://fr.wikipedia.org/wiki/Passager_clandestin_%28%C3%A9conomie%29)).

## Objectifs spécifiques

Les principaux objectifs visés sont les suivants :

-  **Construire** un logiciel en C;
- Décomposer un programme C en **modules**
- Utiliser des **bibliothèques C** existantes;
- Gérer un projet ayant des **dépendances** ;
- Construire des suites de **tests** ;
- **Documenter** un projet pour qu'il soit utilisable.

## Description du travail

Vous devez concevoir un programme qui permet d'afficher des informations sur différents pays. Plus précisément, les informations suivantes doivent être gérées:

- Les langues qui y sont parlées (`show-languages`);
- La capitale (`show-capital`);
- Les pays frontaliers (`show-borders`);

Par défaut, tous les pays sont traités, mais il faut restreindre l'affichage aux paramètres cités ci-dessus : *langues*, *capital* et les *pays frontaliers*.

Votre programme doit inclure les commandes suivantes :

- Commande ``--country COUNTRY``, qui sert à afficher les informations d'un seul pays :

```sh
$ bin/tp3 --country can --show-capital --show-languages --show-borders
Name: Canada
Code: CAN
Capital: Ottawa
Languages: English, French
Borders: USA
```

Pour cette commande vous devez afficher uniquement les paramètres demandés et dans l'ordre saisi (voir exemple ci-dessous).  Par ailleurs vous devez toujours inclure le nom (Name) et le code du pays (le code cca3 dans projet [countries](https://github.com/mledoze/countries)) . Cette règle s'applique pour la prochaine commande (REGION).

```sh
$ bin/tp3 --country can --show-languages --show-capital
Name: Canada
Code: CAN
Languages: English, French
Capital: Ottawa
```
- Commande `--region REGION`, qui affiche seulement les pays d'une région :

```sh
$ bin/tp3 --region oceania --show-capital --show-languages --show-borders

Name: American Samoa
Code: ASM
Capital: Pago Pago
Languages: English, Samoan
Borders:
Name: Australia
Code: AUS
Capital: Canberra
Languages: English
Borders:
...
Name: Samoa
Code: WSM
Capital: Apia
Languages: English, Samoan
Borders:
```
Les pays doivent être affichés selon l'ordre donné dans la base de données [countries](https://github.com/mledoze/countries). Dans l'exemple ci-dessus, à titre explicatif, j'affiche uniquement les deux premiers pays et le dernier séparé par des pointillés (...). Évidemment, votre programme doit afficher tous les pays.

Pour cette commande, vous devez supporter uniquement les régions suivantes :" `africa`", "`americas`", "`asia`", "`europe`" et "`oceania`".

Notez que selon la base de données [countries](https://github.com/mledoze/countries), plusieurs pays d'Océanie ne partagent pas de frontière avec un autre pays, probablement parce qu'il s'agit d'îles.

- La commande `--same-borders`, pour afficher si les pays qui ont des frontières en commun (yes ou no). Cette commande doit supporter jusqu'à 3 pays.

```sh
$ bin/tp3 --same-borders can usa
yes
$ bin/tp3 --same-borders can usa mex
no
$ bin/tp3 --same-borders fra ita che
yes
```
Il faut faire attention à cette commande, il faut que tous les pays partagent une frontière. Dans l'exemple précédent, le Mexique ne partage pas une frontière avec le Canada, c'est pour cette raison que la réponse est non. Pour l'exemple de la France, la Suisse et l'Italie, la réponse est oui car ils se partagent des frontières mutuellement.

- La commande `--same-language`, pour afficher si les pays parlent une ou plusieurs langues  communes (yes ou no), si c'est le cas, vous devez afficher ces langues séparées par un espace entre chacune d'elle. Cette commande doit supporter jusqu'à 3 pays.


```sh
$ bin/tp3 --same-language can usa aus
yes English
```

Les paramètres des commandes ne sont pas sensibles à la casse des lettres (`CAN` ou `can` est la même chose).  Le code [ISO 3166-1 alpha-3](https://en.wikipedia.org/wiki/ISO_3166-1_alpha-3)  sera utilisé pour le code du pays (le code `cca3` dans projet [countries](https://github.com/mledoze/countries)).

## Le projet countries

Les données que vous utiliserez proviennent du projet [countries](https://github.com/mledoze/countries). Celles-ci sont disponibles sous plusieurs formats (JSON, CSV, XML et YAML), mais vous vous concentrerez sur le format JSON. Le fichier que vous utiliserez sera [countries.json](https://github.com/mledoze/countries/blob/master/countries.json) . Ce fichier doit être téléchargé et utilisé en local pour votre application.

## La bibliothèque  `jansson`

Comme pour le tp3, afin de simplifier la correction, je vous impose l'utilisation de la librairie [jansson](http://www.digip.org/jansson/), qui fonctionne bien et bien documentée.

## Structure du projet
Votre projet devra contenir les fichiers suivants :
```
Makefile
bin/
    tp3
data/
    countries.json
src/
    ...
    ...
    tp3.c
test/
    ...
```
- Le fichier `Makefile` doit être à la racine du projet.
- Le code source (fichiers`.c` et `.h`) doivent se retrouver dans le répertoire `src/`;
- L'exécutable (fichier `tp3`) doit se trouver dans le répertoire `bin/`;
- La base de données du projet `country`,  qui est représenté dans le fichier `countries.json` , doit se trouver dans le répertoire `data/`
- La suite de tests doit se trouver dans le répertoire `test/`

## Fichier README

Vous devez inclure un fichier README avec votre projet afin de documenter
différents aspects de celui-ci. Vous devez minimalement inclure les éléments
suivants, mais n'hésitez pas à en ajouter si nécessaire.

```md
# Travail pratique 3

## Description

Description du projet en quelques phrases.
Mentionner le contexte (cours, sigle, université, etc.).

## Auteurs

- Prénom et nom (Code permanent)
- Prénom et nom (Code permanent)
- Prénom et nom (Code permanent)

## Fonctionnement

Expliquez comment fonctionne le programme, quel est son but, quelles sont les
commandes à utiliser, etc.

## Plateformes supportées

Indiquez toutes les plateformes sur lesquelles vous avez testé
l'application, avec la version.

## Dépendances

Décrivez toutes les dépendances de votre projet, autant au niveau des
logiciels (s'il y en a) que des bibliothèques.
Fournissez les liens vers les sites officiels des dépendances si possible.

## Compilation

Expliquez comment compiler et lancer l'application.

## Références

Citez vos sources ici, s'il y a lieu.

## Division des tâches

Donnez ici une liste des tâches de chacun des membres de l'équipe. Utilisez
la syntaxe suivante (les crochets vides indiquent que la tâche n'est pas
complétée, les crochets avec un `X` indique que la tâche est complétée):

- [X] Programmation de la commande --country COUNTRY (Alice)(Charlie)
- [X] Programmation de la commande --region REGION (Charlie)
- [ ] Tests
.
.
## Statut

Indiquer si le projet est complété et sans bogue. Sinon, expliquez ce qui
manque ou ce qui ne fonctionne pas.
```

## Documentation et style

La qualité générale de votre code sera également évaluée. Assurez-vous de respecter les contraintes habituelles suivantes: 
- Les docstrings respectent le standard Javadoc;
- La documentation est bien formatée et bien aérée;
- Le format des docstrings est cohérent, si vous travaillez en équipe.
- La docstring ne contient pas d'erreurs d'orthographe.

##  Tests automatiques

Vous devez gérer les cas de mauvaise utilisation de votre programme de la façon la plus exhaustive possible. Minimalement, il faut respecter les contraintes mentionnées ci-dessus (le format par défaut, les options qui ne s'appliquent pas dans certains cas, etc.), mais vous devez concevoir vous-mêmes des suites de tests couvrant le plus de scénarios possible.

Votre suite de tests doit être lancée à l'aide de la commande

``make test``

Et elle doit afficher le nombre de tests réussis/échoués. Dans le cas d'un test échoué, vous devez donner quelques explications supplémentaires.

Pour la conception de vos tests, vous êtes libres d'utiliser une des deux méthodes suivantes :

- Le projet [CUnit](http://cunit.sourceforge.net/), qui permet de tester directement votre programme en C;
- Un ou plusieurs programmes conçus par vous-même, en C.

##  Contraintes additionnelles

Afin d'éviter des pénalités, il est important de respecter les contraintes suivantes :

- Ne mettez pas tous votre projet dans un seul fichier c.
- Votre main doit être contenue dans un fichier c;
- Vous ne devez pas coder la base de données ``country`` en dure dans votre projet, vous devez utiliser une fonction pour ouvrir le fichier `` countries.json``  afin de charger les données de chaque pays.
- Le nom de votre **exécutable** doit être ``tp3``;
- **Aucune variable globale** (à l'exception des constantes) n'est permise;
- **Aucune bibliothèque (librairie)** autre que les bibliothèques standards C et la bibliothèque ``jansson`` n’est autorisée;
- Il est interdit d'**inclure** un fichier avec l'extension ``.c``, seulement des fichiers avec l'extension ``.h``.
- Chacun de vos modules doit être compilable **séparément** à l'aide d'un ``Makefile``.
- Votre programme doit **compiler** sans **erreur** et sans **avertissement** avec l'option ``-Wall``.

- Le nom de votre projet doit être exactement `inf3135-Hiver2018-tp3`.
- L'URL de votre projet doit être exactement
  `https://gitlab.com/<nom d'utilisateur>/inf3135-Hiver2018-tp3`, où `<nom d'utilisateur>` est l'identifiant GitLab d'un membre de l'équipe.
- Vous devrez donner accès à votre projet en mode **Developer** (pas **Master**) aux utilisateurs `kadouche`, `rmTheZ` et `sim590`. 

Advenant le non-respect d'une ou plusieurs de ces contraintes, une pénalité de
**50%** sera appliquée.

## Remise

Votre travail doit être remis au plus tard le **25 avril 2018** à **23h59**. À partir de minuit, une pénalité de **2% par heure** de retard sera appliquée.
La remise se fait **obligatoirement** par l'intermédiaire de la plateforme [GitLab](https://about.gitlab.com/>) et **moodle**.   **Aucune remise par courriel ne sera acceptée** (le travail sera considéré comme non remis).

Pour la remise sur ***moodle***, compressez, en un fichier (.zip), votre projet selon la structure définie en haut et nommez le **AAAA99999999_TP3.zip** avec votre vrai code permanent.

Les travaux seront corrigés sur le serveur `Java`. Vous devez donc vous assurer que votre programme fonctionne **sans modification** sur celui-ci.

##  Barème

-  Fonctionnalité **50points**:   
	- Affichage des informations, 
	- traitement des paramètres, 
	- lecture du format JSON, etc. 
- Documentations et `Makefile` **15points** : 
	- Style et documentation, décomposition modulaire, encapsulation, etc. 
	- Documentation du fonctionnement (`README`).
	- Compilation, nettoyage, mise à jour de la base de données (`make`, `make clean`).	
- Gestion du projet **15points**: 
  *  La grande majorité des modifications devraient être ajoutées par **requêtes d'intégration**, afin de bien structurer le développement. Notez que le texte qui explique la requête d'intégration n'a pas à être aussi détaillé que pour le 2e travail pratique, une courte explication étant suffisante. 
   * Si vous êtes en **équipe**, vous pouvez désigner un membre unique de l'équipe comme celui qui accepte ou refuse les requêtes d'intégration (il joue le rôle du *release manager*). 
   * Si vous travaillez **seul**, vous devez tout de même structurer vos modifications à l'aide de requêtes d'intégration, que vous accepterez vous-même. C'est sûr que c'est un peu plus artificiel, mais ça permet au moins de bien étudier, rétroactivement, le développement de votre programme. 
   * En particulier, assurez-vous de laisser toutes les branches que vous avez utilisées et fusionnées sur le dépôt afin qu'on puisse les  étudier si nécessaire.
   
- Tests **20points**:  
	- Qualité de la couverture, utilisabilité, lisibilité, fonctionnalité ( `make test`) 
